<?php

/**
 * @file
 * Base block template file for the OOCSS theme.
 *
 * For a list of variables available to you in this template
 * view: http://api.drupal.org/api/file/modules/system/block.tpl.php/6.
 */
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="block basic ">
  <div class="inner">
    <?php if (!empty($block->subject)): ?>
      <div class="hd">
        <em class="h3"><?php print $block->subject ?></em>
      </div>
    <?php endif;?>
    <div class="bd">
      <?php print $block->content ?>
    </div>
  </div>
</div>